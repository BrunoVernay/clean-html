#
# Script to clean up HTML
# 
# Transform URL into links. (Very crude will have to be improved!)
#
# Either pipe a file or put it as first argument

cat "$@" | \
    sed -e "s/&nbsp;/ /g" | \
    sed -e "s#(https?://[^ ]+)#<a href='$1'>$1</a> #g" | \
    tidy -quiet -clean -config tidy.conf | \
    xsltproc -novalid -nonet -html cleanup.xslt -

# sed -e "s/ class=//g" |
