# clean-html
Script to clean up HTML, with Tidy and xsltproc (XSLT)

There is another branch using XQilla (instead of xsltproc), where URL to link transformation is done in XSLT 2.0.

The script uses `tidy` to remove proprietary tags and errors. It also change URL into link (but very crude for now). Then `xsltproc` (XSLT) to remove other attribute or tags, as you want.

XSLT is useful to remove any tag or attribute you don't want. The XSLT fie is easy to customize.


At first I also used `sed` to perform custom operations, before applying `tidy`, to correct eventual problems created with `sed`. 

The encoding is not defined, I have to test to see how it could be an issue.
